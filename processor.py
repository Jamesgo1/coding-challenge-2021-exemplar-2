"""
Spell Checker Processor
---------------------

This is an example of a Lintol processor. You can run it like so:

    python3 processor.py out-example-2021-02-01-hansard-plenary.txt

or, if you would like a nicely-formatted HTML page to look at:

    ltldoorstep -o html --output-file output.html process sample_transcripts/out-example-2021-02-01-hansard-plenary.txt processor.py -e dask.threaded

This will create output.html in the current directory and, in a browser (tested with Chrome), should look like output.png.
"""

import re
import sys
import logging
import spacy
from dask.threaded import get
from spellchecker import SpellChecker

from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.aspect import AnnotatedTextAspect
from ltldoorstep.reports.report import combine_reports
from ltldoorstep.document_utils import load_text, split_into_paragraphs

# We name some cities - we will do all our comparisons in lowercase to match any casing
CITIES = ['armagh', 'belfast', 'derry', 'lisburn', 'newry', 'dublin', 'london', 'brussels']

def spellchecker(text, rprt, nlp):
    """
    Add report items to indicate where spelling mistakes appear
    """

    checker = SpellChecker()

    text = text.replace('--', '')

    # This doorstep utility splits a big text into paragraphs, and standardizes some of
    # the punctuation. We do this splitting so that it is easier to view the results, rather
    # than scrolling through highlighted lines in one big document.
    paragraphs = split_into_paragraphs(text)

    # All characters are Latinate (unicodedata.normalize('NFKD')[0] is Latin)
    docs = nlp.pipe([para[0] for para in paragraphs])
    for para_n, (doc, (paragraph, line_number)) in enumerate(zip(docs, paragraphs)):
        to_check = [tok for tok in doc if tok.pos_ != 'PROPN' and len(tok) > 2]
        failed = checker.unknown(map(str, to_check))
        to_note = [(tok, checker.candidates(str(tok))) for tok in doc if str(tok) in failed]
        if to_note:
            content = AnnotatedTextAspect(doc)
            issue_text = []
            for tok, suggestions in to_note:
                content.add(
                    note=', '.join(suggestions),
                    start_offset=tok.idx,
                    end_offset=tok.idx + len(tok),
                    level=logging.WARNING,
                    tags=['spelling']
                )
                issue_text.append(f'"{tok}"')

            snippet = ''
            if para_n > 0:
                snippet += paragraphs[para_n - 1][0]
            snippet += paragraph
            if para_n < len(paragraphs) - 1:
                snippet += paragraphs[para_n + 1][0]

            rprt.add_issue(
                logging.WARNING,
                'spelling-suggestions',
                f'Spelling issues in paragraph {para_n}: ' + ', '.join(issue_text),
                line_number=line_number,
                character_number=0,
                snippet=snippet,
                content=content
            )

    return rprt

class SpellCheckerProcessor(DoorstepProcessor):
    """
    This class wraps some of the Lintol magic under the hood, that lets us plug
    our spell checker into the online version, and create reports mixing and matching
    from various processors.
    """

    # This is the type of report we create - it could be tabular (e.g. CSV), geospatial
    # (e.g. GeoJSON) or document, as in this case.
    preset = 'document'

    # This is a unique code and version to identity the processor. The code should be
    # hyphenated, lowercase, and start with lintol-code-challenge
    code = 'lintol-code-challenge-spell-checker:1'

    _spacy_model = 'en_core_web_sm'

    # This is a short phrase or description explaining the processor.
    description = "Spell Checker for Lintol Coding Challenge"

    def initialize(self, report=None, context=None):
        self.nlp = spacy.load(self._spacy_model)
        return super().initialize(report, context)

    def get_workflow(self, filename, metadata={}):
        workflow = {
            'load-text': (load_text, filename),
            'get-report': (self.make_report,),
            'step-A': (spellchecker, 'load-text', 'get-report', self.nlp),
            'output': (workflow_condense, 'step-A')
        }
        return workflow

# If there are several steps, this final function pulls them into one big report.
def workflow_condense(base, *args):
    return combine_reports(*args, base=base)

# This is the actual variable Lintol looks for to set up the processor - you
# shouldn't need to touch it (except to change the class name, if neeeded)
processor = SpellCheckerProcessor.make

# Lintol will normally execute this processor in its own magical way, but you
# can also run it via the command line without using ltldoorstep at all (just the
# libraries already imported). The code below lets this happen, and prints out a
# JSON version of the report.
if __name__ == "__main__":
    argv = sys.argv
    processor = SpellCheckerProcessor()
    processor.initialize()
    workflow = processor.build_workflow(argv[1])
    print(get(workflow, 'output'))
